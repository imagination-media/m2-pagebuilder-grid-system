<?php
/**
 * Css grid system for Magento Page Builder by Imagination Media
 *
 * @category  ImaginationMedia
 * @package   ImaginationMedia_PageBuilderGridSystem
 * @author    Luiz Venturote <luiz@imaginationmedia.com>
 * @copyright Copyright (c) 2019 Imagination Media (https://www.imaginationmedia.com/)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'ImaginationMedia_PageBuilderGridSystem',
    __DIR__
);